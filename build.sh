#/bin/bash

result=`git log --oneline $GIT_COMMIT ^$GIT_PREVIOUS_SUCCESSFUL_COMMIT | wc -l`
if [ "$FORCE_BUILD" = "true" ] ; then
    exit 0
elif [ "$result" = "1" ] ; then
    exit -1
else
    exit 0
fi
